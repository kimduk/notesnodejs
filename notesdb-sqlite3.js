var util = require('util');
var sqlite3 = require('sqlite3');
sqlite3.verbose();

var db = undefined;
exports.connect = function(callback) {
    db = new sqlite3.Database("chap3.sqlite3",
    sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
    function(err){
       if(err) {
           util.log("Fail to connect database " + err);
       } else
            callback(null);
    });
}
exports.disconnect = function(callback){
    callback(null);
}

exports.setup = function(callback) {
    db.run("create table if not exists notes (ts datetime, author varchar(255), note text)",
        function(err) {
            if(err) {
                util.log("Error create table " + err);
                callback(error);
            } else {
                callback(null);
            }
        }
    );
}

exports.emptyNote = {ts : "", author : "", note: ""};
exports.add = function(author, note, callback) {
    db.run("insert into notes (ts, author, note) values (?, ?, ?)", [new Date(), author, note],
    function(error) {
        if(error) {
            util.log('FAIL to add ' + error);
            callback(error);
        } else {
            callback(null);
        }
    });
}

exports.delete = function(ts, callback) {
    db.run("delete from notes where ts = ?", [ts], function(err) {
        if(err){
            util.log("DELETE error " + err);
            callback(err);
        } else {
            callback(null);
        }
    });
}

exports.edit = function(ts, author, note, callback) {
    db.run("update note set ts = ? , author = ? , note = ? where ts = ?", [ts, author, note, ts],
        function(err) {
            if(err) {
                util.log("UPDATE error " + err);
                callback(err);
            } else {
                callback(null);
            }
        }
    )
}

exports.allNotes = function(callback) {
    util.log("in allNote");
    db.all('select * from notes', callback);
}

exports.forAll = function(doEach, done) {
    db.each('select * from notes',
        function(err, row) {
            if(err) {
                util.log("FAIL to retrieve row " + err);
                done(err, null);
            } else {
                doEach(null, row);
            }
        },done
    )
}

exports.findNoteById = function(ts, callback) {
    var didOne = false;
    db.run("select * from notes where ts = ?", [ts],
        function(err, row) {
            console.log(row);
            if(err) {
                util.log("FAIL to retrieve row " + err);
                callback(err, null);
            } else {
                if(!didOne) {
                    callback(null, row);
                    didOne = true;
                }
            }
        }
    );
}

